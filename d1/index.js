const txtFirstName=document.querySelector('#txt-first-name');
const txtLastName=document.querySelector('#txt-last-name');
const spanFullName=document.querySelector('#span-full-name'); 

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

//Multiple listeners can also be assigned to the same event
txtFirstName.addEventListener('keyup', (e) => {
	console.log(e.target);
	console.log(e.target.value);//similar to the txtFirstName.value
})


// Mini activity
txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
})
txtLastName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
})

